@extends('template._template')

@section('title') Profile Free User @endsection

@section('content')
    <div>
        <div class="row">
            <div class="col-sm img-fluid">
                <img class="img-position-profile-full"  src="/icons/profile-1.png" alt="Musicos" >
                <div class="position-relative">
                    <img class="img-position-rounded-profile img-profile" src="/icons/profile-rounded.png">
                </div>
            </div>
        </div>
        <div class="row fuente c-white position-relative mt-190">
            <h4 class="ml-160">Tarek Ghanameh</h4><span ><i class="fas fa-1x fa-check-circle ml-5 mt-2 c-red"></i></span>
            <div class="ml-auto mr-3 text-right">
                <button class="btn button-share-profile" ><small class="text-white">Share Profile</small><i class="fas fa-share pl-4 text-white"></i></button>
                <button class="btn ml-2 button-view-profile"><small class="text-white">View Profile</small></button>
            </div>
        </div>
        <div class="row">
            <p class="ml-160">
                <button class="btn text-white w-0 bg-red font-size-10">MIXING ENGINEER</button>
            </p>
        </div>
    </div>
    <div class="row fuente mt-4 ml-2">
        <div class="col-sm-4">
            <ul>
                <div class="text-white">
                    <a href="#"><h5 class="text-white">Personal Details<i class="fas fa-pencil-alt pl-5"></i></h5></a>
                </div>
                <dt>
                   <a class="c-gray"> Name:</a> <a class="text-white">Tarek Ghanameh</a> 
                </dt>
                <dt>
                    <a class="c-gray">Gender:</a> <a class="text-white">Male</a>
                </dt>
                <dt>
                    <a class="c-gray">Location:</a> <a class="text-white">United Kindomg</a>
                </dt>
                <dt>
                    <a class="c-gray">Email:</a> <a class="text-white">t.ghanameh@noisesharks.com</a>
                </dt>
            </ul>
        </div>
        <div class="col-sm-4">
            <ul>
                <div class="text-white">
                    <h5>Suscription </h5>
                </div>
                <dt>
                   <a class="c-gray">Account Type:  </a>
                   <a style="color: #FF0000">Contributor</a> 
                </dt>
                <dt class="pt-2">
                    <button class="btn button-account-setings" ><small class="text-white">Account Setings</small><i class="fas fa-cog pl-4 text-white"></i></button>
                </dt>
            </ul>
        </div>
    </div>
    <div>
        <nav class="navbar navbar-expand-lg navbar-dark bg-black">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse border-top" id="navbarNavAltMarkup">
                <div class="navbar-nav ">
                    <a class="nav-item nav-link c-white" style="border-top: solid 2px #535353" href="#">Profile</a>
                    <a class="nav-item nav-link" style="border-top: solid 2px #FF0000 ;color:#FF0000" href="#">Channel</a>
                </div>
            </div>
        </nav>
    </div>


    <!--Accordion wrapper-->
    <div class="fuente pt-5" id="accordionEx1" role="tablist" aria-multiselectable="true">
        <!-- Accordion card -->
        <div class="card border border-top-0 border-right-0 border-left-0">
            <!-- Card header -->
            <div class="card-header border-gray bg-black" role="tab" id="heading1">
                <div class="collapsed pointer" data-toggle="collapse" data-parent="#accordionEx1" href="#collapse1" aria-expanded="false" aria-controls="collapseTwo1">
                    <h5 class="mb-0 c-gray">Why Support Channel? <small>(Contributors Only)</small></h5>
                    <div class="ml-auto mr-3 text-right">
                        <i class="fas fa-angle-down rotate-icon c-white fa-2x position-relative"></i>
                    </div>
                </div>
            </div>
            <!-- Card body -->
            <div id="collapse1" class="collapse bg-black c-gray" role="tabpanel" aria-labelledby="heading1"
                data-parent="#accordionEx1">
                <div class="card-body bg-black">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
                    wolf
                    moon
                    officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                    Brunch
                    3
                    wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
                    shoreditch
                    et.
                    Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                    Ad
                    vegan
                    excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth
                    nesciunt
                    you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
            </div>
        </div>
        <!-- Accordion card -->
        <!-- Accordion card -->
        <div class="card border-top-0 border-right-0 border-left-0">
            <!-- Card header -->
            <div class="card-header border-gray bg-black pointer" role="tab" id="heading2">
                <div class="collapsed pointer" data-toggle="collapse" data-parent="#accordionEx1" href="#collapse2"
                aria-expanded="false" aria-controls="collapseTwo21">
                <h5 class="mb-0 c-gray">
                    Edit Monthly Goal <small>(Contributors Only)</small>
                </h5>
                <div class="ml-auto mr-3 text-right">
                        <i class="fas fa-angle-down rotate-icon c-white fa-2x position-relative"></i>
                    </div>
                </div>
            </div>
            <!-- Card body -->
            <div id="collapse2" class="collapse bg-black c-gray" role="tabpanel" aria-labelledby="heading2"
                data-parent="#accordionEx1">
                <div class="card-body c-gray">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
                    wolf
                    moon
                    officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                    Brunch
                    3
                    wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
                    shoreditch
                    et.
                    Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                    Ad
                    vegan
                    excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth
                    nesciunt
                    you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
            </div>
        </div>
        <!-- Accordion card -->
        <!-- Accordion card -->
        <div class="card border-top-0 border-right-0 border-left-0">
            <!-- Card header -->
            <div class="card-header border-gray bg-black" role="tab" id="heading3">
                <div class="collapsed pointer" data-toggle="collapse" data-parent="#accordionEx1" href="#collapse3"
                aria-expanded="false" aria-controls="collapse3">
                    <h5 class="mb-0 c-gray">
                        Edit Tiers <small>(Contributors Only)</small>
                    </h5>
                    <div class="ml-auto mr-3 text-right">
                        <i class="fas fa-angle-down rotate-icon c-white fa-2x position-relative"></i>
                    </div>
                </div>
            </div>
            <!-- Card body -->
            <div id="collapse3" class="collapse bg-black c-gray" role="tabpanel" aria-labelledby="heading3"
                data-parent="#accordionEx1">
                <div class="card-body c-gray">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
                    wolf
                    moon
                    officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                    Brunch
                    3
                    wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
                    shoreditch
                    et.
                    Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                    Ad
                    vegan
                    excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth
                    nesciunt
                    you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
            </div>
        </div>
        <!-- Accordion card -->
        <!-- Accordion card -->
        <div class="card border-top-0 border-right-0 border-left-0">
            <!-- Card header -->
            <div class="card-header border-gray bg-black" role="tab" id="heading4">
                <div class="collapsed pointer" data-toggle="collapse" data-parent="#accordionEx1" href="#collapse4"
                aria-expanded="false" aria-controls="collapse4">
                <h5 class="mb-0 c-gray">
                    View Supporters <small>(Contributors Only)</small> <i class="fas fa-angle-right rotate-icon ml-3 c-white"></i>
                </h5>
                </div>
            </div>
            <!-- Card body -->
            <div id="collapse4" class="collapse bg-black c-gray" role="tabpanel" aria-labelledby="heading4"
                data-parent="#accordionEx1">
                <div class="card-body c-gray">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
                    wolf
                    moon
                    officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                    Brunch
                    3
                    wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
                    shoreditch
                    et.
                    Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                    Ad
                    vegan
                    excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth
                    nesciunt
                    you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
            </div>
        </div>
        <!-- Accordion card -->
        <!-- Accordion card -->
        <div class="card border-top-0 border-right-0 border-left-0">
            <!-- Card header -->
            <div class="card-header border-gray bg-black" role="tab" id="heading5">
                <div class="collapsed pointer" data-toggle="collapse" data-parent="#accordionEx1" href="#collapse5"
                aria-expanded="false" aria-controls="collapse5">
                <h5 class="mb-0 c-gray">
                    View Rewards <small>(Contributors Only)</small> <i class="fas fa-angle-right rotate-icon ml-3 c-white"></i>
                </h5>
                </div>
            </div>
            <!-- Card body -->
            <div id="collapse5" class="collapse bg-black c-gray" role="tabpanel" aria-labelledby="heading5" data-parent="#accordionEx1">
                <div class="card-body c-gray">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
                    wolf
                    moon
                    officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                    Brunch
                    3
                    wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
                    shoreditch
                    et.
                    Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                    Ad
                    vegan
                    excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth
                    nesciunt
                    you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
            </div>

        </div>
        <!-- Accordion card -->
        <!-- Accordion card -->
        <div class="card border-top-0 border-right-0 border-left-0">
            <!-- Card header -->
            <div class="card-header border-gray bg-black" role="tab" id="heading6">
                <div class="collapsed pointer" data-toggle="collapse" data-parent="#accordionEx1" href="#collapse6"
                aria-expanded="false" aria-controls="collapse6">
                <h5 class="mb-0 c-white">
                    View Followers <i class="fas fa-angle-right ml-3 c-white rotate-icon"></i>
                </h5>
                </div>
            </div>
            <!-- Card body -->
            <div id="collapse6" class="collapse bg-black c-gray" role="tabpanel" aria-labelledby="heading6"
                data-parent="#accordionEx1">
                <div class="card-body c-gray">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
                    wolf
                    moon
                    officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                    Brunch
                    3
                    wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
                    shoreditch
                    et.
                    Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                    Ad
                    vegan
                    excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth
                    nesciunt
                    you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
            </div>
        </div>
        <!-- Accordion card -->
        <!-- Accordion card -->
        <div class="card border-top-0 border-right-0 border-left-0">
            <!-- Card header -->
            <div class="card-header border-gray bg-black" role="tab" id="heading7">
                <div class="collapsed pointer" data-toggle="collapse" data-parent="#accordionEx1" href="#collapse7"
                aria-expanded="false" aria-controls="collapse7">
                <h5 class="mb-0 c-white">
                    View Supporting <i class="fas fa-angle-right ml-3 c-white rotate-icon"></i>
                </h5>
                </div>
            </div>
            <!-- Card body -->
            <div id="collapse7" class="collapse bg-black c-gray" role="tabpanel" aria-labelledby="heading7"
                data-parent="#accordionEx1">
                <div class="card-body c-gray">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
                    wolf
                    moon
                    officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                    Brunch
                    3
                    wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
                    shoreditch
                    et.
                    Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                    Ad
                    vegan
                    excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth
                    nesciunt
                    you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
            </div>
        </div>
        <!-- Accordion card -->
        <!-- Accordion card -->
        <div class="card border-top-0 border-right-0 border-left-0">
            <!-- Card header -->
            <div class="card-header border-gray bg-black" role="tab" id="heading8">
                <div class="collapsed pointer" data-toggle="collapse" data-parent="#accordionEx1" href="#collapse8"
                aria-expanded="false" aria-controls="collapse8">
                <h5 class="mb-0 c-white">
                    View Rewarded <i class="fas fa-angle-right ml-3 c-white rotate-icon"></i>
                </h5>
                </div>
            </div>
            <!-- Card body -->
            <div id="collapse8" class="collapse bg-black c-gray" role="tabpanel" aria-labelledby="heading8"
                data-parent="#accordionEx1">
                <div class="card-body c-gray">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
                    wolf
                    moon
                    officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                    Brunch
                    3
                    wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
                    shoreditch
                    et.
                    Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                    Ad
                    vegan
                    excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth
                    nesciunt
                    you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
            </div>
        </div>
        <!-- Accordion card -->
        <!-- Accordion card -->
        <div class="card border-top-0 border-right-0 border-left-0">
            <!-- Card header -->
            <div class="card-header border-gray bg-black" role="tab" id="heading9">
                <div class="collapsed pointer" data-toggle="collapse" data-parent="#accordionEx1" href="#collapse9"
                aria-expanded="false" aria-controls="collapse9">
                <h5 class="mb-0 c-white">
                    View Fllowing<i class="fas fa-angle-right ml-3 c-white rotate-icon"></i>
                </h5>
                </div>
            </div>
            <!-- Card body -->
            <div id="collapse9" class="collapse bg-black c-gray" role="tabpanel" aria-labelledby="heading9"
                data-parent="#accordionEx1">
                <div class="card-body c-gray">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
                    wolf
                    moon
                    officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                    Brunch
                    3
                    wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
                    shoreditch
                    et.
                    Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                    Ad
                    vegan
                    excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth
                    nesciunt
                    you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
            </div>
        </div>
    </div>
    <!-- Accordion wrapper -->
    <div class="row text-center">
        <div class="col-sm pb-2 mt-2" style="margin-bottom:100px">
            <button class="btn bg-black c-white">cancel</button>
            <button class="btn bg-red c-white">save</button>
        </div>
    </div>
</div>
@endsection
