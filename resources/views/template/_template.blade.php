<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Noise Sharks - @yield('title')</title>
        <link rel="stylesheet" href="{{ mix('/css/app.css') }}"> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        @yield('css')
    </head>
    <body>
        @include('template._header')
        <div class="app-body">
            @include('template._nav')
            <main class="main animated fadeIn bg-black">
                @yield('content')
            </main>
        </div>
        @include('template._footer')
        <script src="{{ mix('/js/app.js') }}"></script>
        @stack('scripts')
        @yield('js')
    </body>
</html>