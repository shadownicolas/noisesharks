<nav class="bg-gradiente">
    <ul class="text-white  fuente">
        <div class="pt-3">
            <table>
                <thead>
                    <tr>
                        <td><h6 class="pl-3">Following </h6></td> 
                        <th><i class="fas fa-star text-white"></i></th>
                    </tr>
                    <tr>
                        <td colspan="2" class="pb-4">
                            <img src="/icons/icon-line.svg" alt="Linea Shark" height="15px">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><h5 class="pt-3">Genre</h5></td>
                    </tr>
                    <tr>
                        <td>pop</td>
                        <th>
                            <input type="checkbox" class="checkbox" id="check1">
                            <label for="check1"></label>
                        </th>
                    </tr>
                    <tr>
                        <td>Rap & Hip-Hop</td>
                        <th>
                            <input type="checkbox" class="checkbox" id="check2">
                            <label for="check2"></label>
                        </th>
                    </tr>
                    <tr>
                        <td>EDM</td>
                        <th>
                            <input type="checkbox" class="checkbox" id="check3">
                            <label for="check3"></label>
                        </th>
                        
                    </tr>
                    <tr>
                        <td>Rock & Metal</td>
                        <th>
                            <input type="checkbox" class="checkbox" id="check4">
                            <label for="check4"></label>
                        </th>
                        
                    </tr>                       
                    <tr>
                        <td>Jazz & Blues</td>
                        <th>
                            <input type="checkbox" class="checkbox" id="check5">
                            <label for="check5"> </label>
                        </th>
                        
                    </tr>              
                    <tr>
                        <td>Clasical</td>
                        <th>
                            <input type="checkbox" class="checkbox" id="check6">
                            <label for="check6"> </label>
                        </th>
                        
                    </tr>
                    <tr>
                        <td>Punk</td>
                        <th>
                            <input type="checkbox" class="checkbox" id="check7">
                            <label for="check7"></label>
                        </th>
                        
                    </tr>
                    <tr>
                        <td>World</td>
                        <th>
                            <input type="checkbox" class="checkbox" id="check8">
                            <label for="check8"></label>
                        </th>
                        
                    </tr>
                    <tr>
                        <td><h5 class="pt-4">Category</h5></td>
                    </tr>
                    <tr>
                        <td>Production & Engennery</td>
                        <th>
                            <input type="checkbox"class=" checkbox" id="check9">
                            <label for="check9"></label>
                        </th>
                        
                    </tr>
                    <tr>
                        <td>Preforming</td>
                        <th>
                            <input type="checkbox"class=" checkbox" id="check10">
                            <label for="check10"></label>
                        </th>
                        
                    </tr>
                    <tr>
                        <td>Blogs</td>
                        <th>
                            <input type="checkbox" class="checkbox" id="check11">
                            <label for="check11"> </label>
                        </th>
                        
                    </tr>
                    <tr>
                        <td>Instruments</td>
                        <th>
                            <input type="checkbox"class="checkbox" id="check12">
                            <label for="check12"></label>
                        </th>
                        
                    </tr>
                    <tr>
                    </tr>
                        <td>Podcast</td>
                        <th>
                            <input type="checkbox" class="checkbox" id="check13">
                            <label for="check13"> </label>
                        </th>
                        
                    <tr>
                        <td>Audio Chips</td>
                        <th>
                            <input type="checkbox" class="checkbox" id="check13">
                            <label for="check13"></label>
                        </th>
                        
                    </tr>
                </thead>
            </table>
        </div>
        <div style="color: #212121">
            ________________________
        </div>
        <div class="pt-3">
            <table>
                <thead>
                    <tr>
                        <td><h6 class="pl-3">Following </h6></td> 
                        <th><i class="fas fa-star text-white"></i></th>
                    </tr>
                    <tr>
                        <td colspan="2" class="pb-4">
                            <img src="/icons/icon-line.svg" alt="Linea Shark" height="15px">
                        </td>
                    </tr>
                    <tr>
                        <td class="nav-item p-1 pointer">SoundStorming  <span ><i class="fas fa-4px fa-check-circle ml-3 c-red"></i></span> </td>
                        <th class="c-red">•</th>
                    </tr>
                    <tr>
                        <td class="nav-item p-1 pointer">Musicinfo.io a </td>
                        <th class="c-red">• </th>
                    </tr>
                    <tr>
                        <td class="nav-item p-1 pointer">Dan Korneff  <span ><i class="fas fa-4px fa-check-circle ml-3 c-red"span> </td>
                        <th class="c-red">•</th>
                    </tr>
                    <tr>
                        <td class="nav-item p-1 pointer">Andrew Wade </td>
                        <th class="c-red">•</th>
                    </tr>
                    <tr>
                        <td class="nav-item p-1 pointer">Tarek Ghanameh  <span ><i class="fas fa-4px fa-check-circle ml-3 c-red"><span> </td>
                        <th class="c-red">•</th>
                    </tr>
                    <tr>
                        <td class="nav-item p-1 pointer">Karim Hadaya </td>
                        <th class="c-red">•</th>
                    </tr>
                    <tr>
                        <td class="nav-item p-1 pointer">Sham A </td>
                        <th class="c-red">•</th>
                    </tr>
                    <tr>
                        <td class="nav-item p-1 pointer">Joey Sturgies </td>
                        <th class="c-red">•</th>
                    </tr>
                    <tr>
                        <td class="nav-item p-1 pointer c-gray">Native Instruments </td>
                        <th class="c-blue" >•</th>
                    </tr>
                    <tr>
                        <td class="nav-item p-1 pointer c-gray">Splice </td>
                        <th class="c-blue" >•</th>
                    </tr>
                    <tr>
                        <td class="nav-item p-1 pointer c-gray">Steingberg </td>
                        <th class="c-blue">•</th>
                    </tr>

                </thead>
            </table>
        </div>
        <div class="pt-3">
            <table class="table table-hover">
                
            </table>
        </div>
        <div style="color: #212121">
            ________________________
        </div>
        <div class="row">
            <div class="col col-sm-4">
                <a href="#" class="link">Terms</a>
            </div>
            <div class="col col-sm-4">
                <a href="#" class="link">Privacity</a>
            </div>
            <div class="col col-sm-4">
                <a href="#" class="link">Help</a>
            </div>
            
        </div>
        <div class="row">
            <div class="col-sm-4">
                <a href="#" class="link">Sugestions</a>
            </div>
            <div class="col-sm-4">
                <a href="#" class="link">Contact</a>
            </div>
            <div class="col-sm-4">
                <a href="#" class="link">About</a>
            </div>
            
        </div>
        <div class="row">
            <div class="col col-sm-3">
                <a href="#" class="link">Carrers</a>
            </div>
            <div class="col col-sm-3">
                <a href="#" class="link">Merch</a>
            </div>
        </div>
        <div class="row">
            <div class="col col-sm">
                <a href="#" class="link">For Music Companies</a>
            </div>
        </div>
        <a href="/" class="font pt-3">
           <small>&copy; 2019 Noisesharks ® Registered in England and Wales No. 10670071</small> 
        </a>
    </ul>
</nav>