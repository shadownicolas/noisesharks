<header class="app-header navbar p-0" style="height: 58px;">
    <a class="navbar-brand" href="/">
        <img class="navbar-brand-ful" src="/icons/icon-defined.png"  alt="Noise Sharks">
    </a>
    <div class="mt-3" style="margin-left: 93px">
        <a href="/">
            <p class="sub-g fuente">Sream</p>
        </a>
    </div>
    <div class="ml-3 mt-3">
        <a href="/">
            <p class="sub c-white fuente">Profile</p>
        </a>
    </div>
    <ul class="nav navbar-nav ml-auto">
        <li>
            <form class="form-inline my-2 my-lg-0">
                <div class="input-group md-form form-sm form-2 pl-0">
                    <input class="form-control my-0 py-1 gray-border bg-dark btn-sm "  style="background: #121212; width: 230px" type="text" placeholder="Search  for post, hashtag, etc..." aria-label="Search">
                    <div class="input-group-append">
                        <span class="input-group-text lighten-3 fuente"  style="border-radius: 0px 30px 30px 0px; background: #121212"><i class="fas fa-search text-gray" aria-hidden="true"></i></span>
                    </div>
                    <div>
                        <input class="mt-1 ml-3 btn btn-sm btn-outline-secondary fuente" type="button" value="Become A Contributor" style="border-radius: 30px; background: #121212; color: white;" placeholder="Become A Contributor">
                    </div>
                </div>
            </form>
        </li>
        <li class="nav-item dropdown pr-2">
            <a class="nav-link ml-4 mr-4" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <img class="avatar "  src="/icons/icon.png"></a>
            <div class="dropdown-menu dropdown-menu-right bg-black ">
                <a class="dropdown-item c-white" href="#"><i class="fa fa-user"></i> Profile</a>
                <a class="dropdown-item c-white" href="#"><i class="fa fa-lock"></i> Logout</a>
            </div>
        </li>
    </ul>
</header>
